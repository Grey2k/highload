https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04

sudo apt update
sudo apt install mysql-server
sudo mysql_secure_installation
sudo mysql


CREATE USER 'highload'@'localhost' IDENTIFIED BY 'Pa$$w0rd!';

create database hl;

GRANT CREATE, ALTER, DROP, INSERT, UPDATE, DELETE, SELECT, REFERENCES, RELOAD on *.* TO 'highload'@'localhost' WITH GRANT OPTION;

flush privileges;

exit;


systemctl status mysql.service


sudo apt-get install libmysqlclient-dev

export FLASK_APP=project
export FLASK_DEBUG=1
flask run
