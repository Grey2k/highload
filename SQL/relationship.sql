CREATE TABLE IF NOT EXISTS relationship (
    relating_user_id INT NOT NULL,
    related_user_id INT NOT NULL,
    UNIQUE (relating_user_id, related_user_id),
    INDEX (relating_user_id)
)