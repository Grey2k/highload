from flask import Blueprint, render_template, redirect, url_for, request, session
from werkzeug.security import generate_password_hash, check_password_hash
from MySQLdb import escape_string as thwart
from project import models

auth = Blueprint('auth', __name__)

@auth.route('/index')
def index():
    if 'logged_in' in session:
        # here later will be friends posts feed
        return render_template('index.html')
    else:
        return render_template('index.html')

@auth.route('/login')
def login():
    login_failed = request.args.get('failed') != None
    if (login_failed):
        return render_template('login.html', login_failed=1)
    else:
        return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    username = thwart(request.form.get('username'))
    password = request.form.get('password')

    try:
        c, conn = models.connection()

        x = c.execute("SELECT user_name, password FROM users WHERE user_name = %(username)s", {'username': username.decode('utf-8')})

        if int(x) == 1:
            password_hash = c.fetchone()[1]
            if (check_password_hash(password_hash, password)):
                session['logged_in'] = True
                session['username'] = username
                c.close()
                conn.close()
                return redirect(url_for('main.profile'))
            else:
                c.close()
                conn.close()
                return redirect(url_for('auth.login', failed=1))
        else:
            c.close()
            conn.close()
            return redirect(url_for('auth.login', failed=1))
    except Exception as e:
        print (str(e))
        return (str(e))


@auth.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('username', None)
    return render_template('index.html')


@auth.route('/signup')
def signup():
    return render_template('signup.html')

@auth.route('/signup', methods=['POST'])
def signup_post():
    username = request.form.get('username')
    password = request.form.get('password')

    if (len(username) == 0 or len(password) == 0):
        return render_template('signup.html', signup_failed = 2)

    username = thwart(username)
    password = generate_password_hash(password, method='sha256')

    try:
        c, conn = models.connection()

        x = c.execute("SELECT * FROM users WHERE user_name = %(username)s", {'username':username.decode('utf-8')})

        if int(x) > 0:
            c.close()
            conn.close()
            return render_template('signup.html', signup_failed = 1)
        else:
            c.execute("INSERT INTO users (user_name, password) VALUES (%(username)s, %(password)s)",
                      {
                          'username':username.decode('utf-8'),
                          'password':password
                      }
                     )

            conn.commit()
            c.close()
            conn.close()

            session['logged_in'] = True
            session['username'] = username

            return redirect(url_for('auth.index'))

        c.close()
        conn.close()

    except Exception as e:
        print (str(e))
        return (str(e))


