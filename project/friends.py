from flask import Blueprint, render_template, redirect, url_for, request, session
from MySQLdb import escape_string as thwart
from project import models

friends = Blueprint('friends', __name__)

@friends.route('/search_friends')
def search():
    if ('logged_in' in session):
        current_user = session['username']
        first_name =thwart(models.xstr(request.args.get('first_name')))
        last_name = thwart(models.xstr(request.args.get('last_name')))
        age = thwart(models.xstr(request.args.get('age')))
        gender = thwart(models.xstr(request.args.get('gender')))
        interests = thwart(models.xstr(request.args.get('interests')))
        city = thwart(models.xstr(request.args.get('city')))
        page_n = thwart(models.xstr(request.args.get('page_n')))
        users_per_page = 3
        pages_offset = 2

        args_dict = request.args.to_dict()
        if 'page_n' in args_dict:
            del args_dict['page_n']

        if (len(args_dict) == 0):
            return render_template("search_friends.html")

        try:
            c, conn = models.connection()
            d = dict()

            #q =  "SELECT user_id, user_name, full_name, city FROM users WHERE 1=1 AND user_name != %(user_name)s "


            # переделать вместо birth_date на age
            qs = "SELECT user_id, user_name, concat (last_name, ' ', first_name) as full_name, TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) as AGE, gender, interests, city "
            qf = "FROM users "
            qw = "WHERE 1=1 AND user_name != %(user_name)s "


            d['user_name'] = current_user.decode('utf-8')

            first_name = models.get_str_for_search_sql('first_name', first_name)
            if (first_name['first_name'] != ""):
                qw = qw + first_name['first_name'][0]
                d['first_name'] = first_name['first_name'][1]

            last_name = models.get_str_for_search_sql('last_name', last_name)
            if (last_name['last_name'] != ""):
                qw = qw + last_name['last_name'][0]
                d['last_name'] = last_name['last_name'][1]

            interests = models.get_str_for_search_sql('interests', interests)
            if (interests['interests'] != ""):
                qw = qw + interests['interests'][0]
                d['interests'] = interests['interests'][1]

            city = models.get_str_for_search_sql('city', city)
            if (city['city'] != ""):
                qw = qw + city['city'][0]
                d['city'] = city['city'][1]

            gender = models.get_str_for_strict_search_sql('gender', gender)
            if (gender['gender'] != ""):
                qw = qw + gender['gender'][0]
                d['gender'] = gender['gender'][1]

            age = models.get_int_age_for_strict_search_sql('birth_date', age)
            if (age['birth_date'] != ""):
                qw = qw + age['birth_date'][0]
                d['birth_date'] = age['birth_date'][1]

            q = qs + qf + qw + f" ORDER BY CONCAT(last_name, ' ', first_name) LIMIT %s, %s" % (models.get_str_for_int(models.get_int_for_str(page_n, "hard")* users_per_page), str(users_per_page))

            x = c.execute(q, d)

            if (int(x) == 0):
                c.close()
                conn.close()
                return render_template ("search_friends.html", no_found=1)
            else:
                users = c.fetchall()

                x1 = c.execute("select count(1) " + qf + qw, d)
                n_rec = c.fetchone()[0]

                pages = models.get_page_navigation(models.get_int_for_str(page_n),
                                                   n_rec,
                                                   users_per_page,
                                                   pages_offset,
                                                   args_dict,
                                                   uri="search_friends")
                c.close()
                conn.close()
                return render_template ("search_friends.html", users=users, pages = pages)
        except Exception as e:
            print(str(e))
            return (str(e))
    else:
        return redirect(url_for('main.index'))

@friends.route('/show_profile')
def show_profile():
    if ('logged_in' in session):
        current_user = session['username']
        id = models.get_int_for_str(thwart(models.xstr(request.args.get('id'))),"soft")
        friend_action = f"%s" % thwart(models.xstr(request.args.get('friend'))).decode('utf-8')

        if (id == None):
            return render_template ("show_profile.html", no_found=1)

        args_dict = request.args.to_dict()
        if 'friend' in args_dict:
            del args_dict['friend']

        self_id = models.get_user_id(current_user)

        if (id == self_id):
            return redirect(url_for('main.profile'))


        id = models.get_str_for_int(id)

        try:
            c, conn = models.connection()

            action = ""
            if (friend_action == 'add'):
                q = "INSERT INTO relationship (relating_user_id, related_user_id) " \
                    "VALUES (%(relating_user_id)s, %(related_user_id)s), (%(related_user_id)s, %(relating_user_id)s) "
                d = {"relating_user_id":self_id, "related_user_id":id}
                c.execute(q, d)
                conn.commit()
                action = "added"

            if (friend_action == 'delete'):
                q = "DELETE FROM relationship " \
                    "WHERE (relating_user_id, related_user_id) IN " \
                    "((%(relating_user_id)s, %(related_user_id)s), (%(related_user_id)s, %(relating_user_id)s)) "
                d = {"relating_user_id":self_id, "related_user_id":id}
                c.execute(q, d)
                conn.commit()
                action = "deleted"

            q =  "SELECT user_id, user_name, concat (last_name, ' ', first_name) as full_name, TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) as AGE, gender, interests, city FROM users WHERE user_id = %(user_id)s "

            x = c.execute(q, {"user_id":id})

            if (int(x) == 0):
                c.close()
                conn.close()
                return render_template ("show_profile.html", no_found=1)
            else:
                user = c.fetchone()

                c.close()
                conn.close()

                relationship_status = models.get_relationship_status(self_id, id)
                relationship_button = models.get_relationship_button(id, relationship_status, args_dict)

                users_per_page = 3
                friends_data = models.get_friends_info(user[1], start=0,
                                                       limit=users_per_page, mode="limit", self_user_id=self_id)
                friends_total = models.get_friends_info(user[1], mode="all", self_user_id=self_id)[0][0]

                if (friends_total == 0):
                    return render_template ("show_profile.html", user=user, relationship_button=relationship_button, action=action, no_friend=1)
                else:
                    return render_template ("show_profile.html", user=user, relationship_button=relationship_button, action=action, users=friends_data)
        except Exception as e:
            print (str(e))
            return render_template ("search_friends.html")
    else:
        return redirect(url_for('main.index'))

@friends.route('/show_friends')
def show_friends():
    if ('logged_in' in session):
        page_n = thwart(models.xstr(request.args.get('page_n')))
        self_id = models.get_user_id(session['username'])

        id = models.get_int_for_str(thwart(models.xstr(request.args.get('id'))), "soft")
        if (id == None):
            current_user = session['username']
        else:
            current_user = models.get_user_name(id)

        args_dict = request.args.to_dict()
        if 'page_n' in args_dict:
            del args_dict['page_n']

        users_per_page = 3
        pages_offset = 2
        friends_data = models.get_friends_info(current_user,
                                               start=models.get_int_for_str(page_n, mode="hard"),
                                               limit=users_per_page,
                                               mode="limit",
                                               self_user_id=self_id)
        friends_total = models.get_friends_info(current_user, mode="all",self_user_id=self_id)[0][0]
        if (friends_total == 0):
            return render_template("show_friends.html", no_found = 1)
        else:
            pages = models.get_page_navigation(models.get_int_for_str(page_n),
                                               friends_total,
                                               users_per_page,
                                               pages_offset,
                                               query = args_dict,
                                               uri="show_friends")
            return render_template("show_friends.html", users=friends_data, pages=pages)
    else:
        return redirect(url_for('main.index'))