from flask import Blueprint, render_template, redirect, url_for, request, session
from MySQLdb import escape_string as thwart
from project import models

main = Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template('index.html')

@main.route('/profile')
def profile():
    if ('logged_in' in session):
        profile_data = models.get_profile_info(session['username'])
        friends_data = models.get_friends_info(session['username'])
        return render_template('profile.html', profile_data=profile_data, friends_data=friends_data, func = models.get_date_str)
    else:
        return redirect(url_for('main.index'))

@main.route('/profile', methods=['POST'])
def profile_post():
    if ('logged_in' in session):
        first_name = thwart(request.form.get('first_name'))
        last_name = thwart(request.form.get('last_name'))
        birth_date = models.get_date_str(thwart(request.form.get('birth_date')))
        gender = thwart(request.form.get('gender'))
        interests = thwart(request.form.get('interests'))
        city = thwart(request.form.get('city'))

        try:
            c, conn = models.connection()

            x = c.execute("SELECT 1 FROM users WHERE user_name = %(username)s", {'username':session['username'].decode('utf-8')})

            if int(x) == 1:
                c.execute("UPDATE users SET first_name = %(first_name)s, \
                                            last_name = %(last_name)s, \
                                            birth_date = STR_TO_DATE(%(birth_date)s, \"%%d/%%m/%%Y\"), \
                                            gender = %(gender)s, \
                                            interests = %(interests)s, \
                                            city = %(city)s WHERE user_name = %(user_name)s " ,
                            {
                                'first_name':first_name.decode('utf-8'),
                                'last_name': last_name.decode('utf-8'),
                                'birth_date': birth_date,
                                'gender': gender.decode('utf-8'),
                                'interests': interests.decode('utf-8'),
                                'city':city.decode('utf-8'),
                                'user_name':session['username'].decode('utf-8')
                            }
                          )

                conn.commit()

                c.close()
                conn.close()

                profile_data = models.get_profile_info(session['username'])
                friends_data = models.get_friends_info(session['username'])
                return render_template('profile.html', profile_data=profile_data, friends_data=friends_data, func = models.get_date_str)

            else:
                c.close()
                conn.close()
                return redirect(url_for('main.index'))
        except Exception as e:
            print (str(e))
            return (str(e))

        profile_data = models.get_profile_info(session['username'])
        friends_data = models.get_friends_info(session['username'])
        return render_template('profile.html', profile_data=profile_data, friends_data=friends_data)
    else:
        return redirect(url_for('main.index'))
