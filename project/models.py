import MySQLdb
from dateutil.parser import parse
import os

def connection():
    host = os.environ['MYSQL_HOST']
    user = os.environ['MYSQL_USER']
    password = os.environ['MYSQL_PASSWORD']
    db = os.environ['MYSQL_DB']

    conn = MySQLdb.connect(host=host,
                           user=user,
                           passwd=password,
                           db=db)
    c = conn.cursor()

    return c, conn


def xstr(s):
    if s is None:
        return ''
    return str(s)


def xstr_arr(arr):
    return [xstr(x) for x in arr]


def get_profile_info(user_name: str):
    c, conn = connection()

    x = c.execute("SELECT first_name, last_name, DATE_FORMAT(birth_date,'%%d/%%m/%%Y') AS birth_date, gender, interests, city FROM users WHERE user_name = %(user_name)s ", {'user_name':user_name.decode('utf-8')})
    data = c.fetchone()

    c.close()
    conn.close()
    if (int(x) == 1):
        return xstr_arr(data)
    else:
        return [str(''), str('')]


def get_str_for_int(s: str):
    try:
        i = int(s)
        if (i >= 0):
            return str(i)
        else:
            return str(0)
    except ValueError:
        return str(0)

def get_int_for_str(s: str, mode="hard"):
    try:
        i = int(s)
        return i
    except ValueError:
        if(mode=="soft"):
            return None
        else:
            return 0






def get_str_for_search_sql(field_name: str, search_value: str):
    d = dict()
    if (len(field_name) == 0):
        d[field_name] = ""
        return d
    else:
        if (len(search_value.strip()) == 0):
            d[field_name] = ""
            return d
        else:
            d[field_name] = [f" AND lower(%s) LIKE %%(%s)s " % (field_name, field_name), f"%%%s%%" % search_value.strip().lower().decode('utf-8')]
            return d


def get_str_for_strict_search_sql(field_name: str, search_value: str):
    d = dict()
    if (len(field_name) == 0):
        d[field_name] = ""
        return d
    else:
        if (len(search_value.strip()) == 0):
            d[field_name] = ""
            return d
        else:
            d[field_name] = [f" AND lower(%s) = %%(%s)s " % (field_name, field_name),\
                             search_value.strip().lower().decode('utf-8')]
            return d

            ##
def get_int_age_for_strict_search_sql(field_name: str, search_value: str):
    d = dict()
    if (len(field_name) == 0):
        d[field_name] = ""
        return d
    else:
        if (len(search_value.strip()) == 0 or get_int_for_str(search_value.strip(), mode='soft') == None):
            d[field_name] = ""
            return d
        else:
            d[field_name] = [f" AND TIMESTAMPDIFF(YEAR, %s, CURDATE()) = %%(%s)s " % (field_name, field_name), \
                             str(get_int_for_str(search_value.strip(), mode='soft'))]
            return d

def get_url_from_dict(d:dict):
    return '&'.join([f'{key}={value}' for key, value in d.items()])

def get_text_or_href(t:str, h:str, page_n:int, i:int):
    if (page_n == i):
        return t
    else:
        return h

def get_page_navigation(page_n: int, row_cnt: int, users_per_page: int, pages_offset: int, query = dict(), uri="search_friends"):
    # todo: добавить стили https://bulma.io/documentation/components/pagination/
    s = ""

    if (row_cnt % users_per_page == 0):
        max_page = row_cnt // users_per_page
    else:
        max_page = (row_cnt // users_per_page) + 1

    s += get_text_or_href("<span class=\"pagination-link is-current\">1</span> ",
                          f"<a class=\"pagination-link\" href=/%s?%s&page_n=%i>%i</a> " % (uri, get_url_from_dict(query), 0, 1),
                          page_n,
                          0)

    if (page_n - pages_offset >= 1):
        s += "<span class=\"pagination-ellipsis\">&hellip;</span>"

    start_page = max (2, page_n - pages_offset + 2)
    end_page   = min (max_page-1, page_n + pages_offset)

    for i in range(start_page, end_page+1):
        s += get_text_or_href(f"<span class=\"pagination-link is-current\">%i</span> " % i,
                          f"<a class=\"pagination-link\" href=/%s?%s&page_n=%i>%i</a> " % (uri, get_url_from_dict(query), i-1, i),
                          page_n,
                          i-1)

    if (page_n + pages_offset < max_page - 1):
        s += "<span class=\"pagination-ellipsis\">&hellip;</span>"

    if (max_page != 1):
        s += get_text_or_href(f"<span class=\"pagination-link is-current\">%i</span> " % (max_page) ,
                          f"<a class=\"pagination-link\" href=/%s?%s&page_n=%i>%i</a> " % (uri, get_url_from_dict(query), (max_page-1), max_page),
                          page_n,
                          max_page-1)

    return s

def get_relationship_status (relating_user_id: int, related_user_id: int):
    c, conn = connection()

    x = c.execute("SELECT 1 FROM relationship WHERE relating_user_id = %(relating_user_id)s and related_user_id = %(related_user_id)s ",
                  {'relating_user_id': relating_user_id, 'related_user_id': related_user_id})
    c.close()
    conn.close()

    if (int(x) == 1):
        return True
    else:
        return False

def get_user_id (user_name:str):
    c, conn = connection()

    x = c.execute(
        "SELECT user_id FROM users WHERE user_name = %(user_name)s ",
        {'user_name': user_name})
    result = c.fetchall()

    c.close()
    conn.close()

    if (len(result) != 1):
        return -1
    else:
        return result[0][0]


def get_relationship_button (user_id:int,
                             relationship_status:bool,
                             url:dict):
    if (relationship_status):
        return f' <b color="green">friends</b> <a style="color:blue;text-decoration: underline;" href=/show_profile?%s&friend=delete>unfriend</a>' % (get_url_from_dict(url))
    else:
        return f' <b color="black">not friends</b> <a style="color:blue;text-decoration: underline;" href=/show_profile?%s&friend=add>add a friend</a>' % (get_url_from_dict(url))

def get_friends_info(user_name: str, start=0, limit=10, mode="limit", self_user_id = -1):
    c, conn = connection()

    d = dict()
    d['user_name'] = user_name
    d['self_user_id'] = self_user_id

    q1 = "FROM users u_self\
        JOIN relationship r\
        ON u_self.user_id = r.relating_user_id\
        JOIN users u_fr\
        ON r.related_user_id = u_fr.user_id\
        WHERE u_self.user_name = %(user_name)s\
        AND r.related_user_id != %(self_user_id)s "

    if (mode == "limit"):
        d['start'] = start
        d['limit'] = limit

        q = "SELECT u_fr.user_id, CONCAT(u_fr.last_name, ' ', u_fr.first_name) AS full_name, TIMESTAMPDIFF(YEAR,\
         u_fr.birth_date, CURDATE()) as AGE, u_fr.gender, u_fr.interests, u_fr.city " + q1 + " LIMIT %(start)s, %(limit)s"
    else:
        q = "SELECT count(1) " + q1

    x = c.execute(q, d)
    result = c.fetchall()

    c.close()
    conn.close()

    return result

def get_user_name (user_id:int):
    c, conn = connection()

    x = c.execute(
        "SELECT user_name FROM users WHERE user_id = %(user_id)s ",
        {'user_id': user_id})
    result = c.fetchone()

    c.close()
    conn.close()

    if (result == None):
        return ""
    else:
        return result[0]

def get_date_str (input_date:str):
    try:
        output_date = parse(input_date, fuzzy=False, dayfirst=True)
        return output_date.strftime("%d/%m/%Y")
    except ValueError:
        return '01/01/9999'
